﻿using System;
using System.Globalization;
using SignalRApplication.Bll.Models;
using TIBCO.Rendezvous;

namespace SignalRApplication.Bll.TIBcoRv
{
    public class MessageParser: ITibcoMessageParser
    {
        public  Market GetModelFromMesssage(Message message)
        {            
            var  updatedMarket = new Market
            {
                Id = Convert.ToInt32(message.GetField("MarketId").Value),
                Bid = Convert.ToDouble(message.GetField("Bid").Value, CultureInfo.InvariantCulture),
                Offer = Convert.ToDouble(message.GetField("Offer").Value, CultureInfo.InvariantCulture),
                Direction = Convert.ToBoolean(message.GetField("Direction").Value),
            };

            return updatedMarket;
        }
    }
}
