﻿using SignalRApplication.Bll.Models;
using TIBCO.Rendezvous;

namespace SignalRApplication.Bll.TIBcoRv
{
    public interface ITibcoMessageParser
    {
        Market GetModelFromMesssage(Message message);
    }
}
