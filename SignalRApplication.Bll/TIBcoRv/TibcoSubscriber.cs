﻿using SignalRApplication.Bll.Models;
using SignalRApplication.Bll.Utils;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using TIBCO.Rendezvous;

namespace SignalRApplication.Bll.TIBcoRv
{
    public class TibcoSubscriber : IDisposable
    {
        private readonly string _daemon;
        private readonly string _service;
        private readonly string _network;
        private Transport _transport;
        private readonly CancellationTokenSource _listenerCancellationToken;
        readonly ConcurrentDictionary<string, List<MarketParserModel>> _topicSubscribers = new ConcurrentDictionary<string, List<MarketParserModel>>();

        public TibcoSubscriber(string daemon, string service, string network)
        {
            _daemon = daemon;
            _service = service;
            _network = network;
            _listenerCancellationToken = new CancellationTokenSource();
        }

        public void StartTibco()
        {
            TIBCO.Rendezvous.Environment.Open();
            _transport = new NetTransport(_service, _network, _daemon);

            Task.Run(() => RunListner(), _listenerCancellationToken.Token);
        }

        public void AddListners(string topic, Delegates.MarketReceivedEventHandler marketReceivedHandler, ITibcoMessageParser parser)
        {
            var listner = new Listener(Queue.Default, _transport, topic, null);

            if (!_topicSubscribers.ContainsKey(topic))
            {
                var marketParserModels = new List<MarketParserModel>
                {
                    new MarketParserModel
                    {
                        Parser = parser,
                        ReceivedHandler = marketReceivedHandler
                    }
                };

                _topicSubscribers.TryAdd(topic, marketParserModels);
            }
            else
            {
                _topicSubscribers[topic].Add(new MarketParserModel
                {
                    Parser = parser,
                    ReceivedHandler = marketReceivedHandler
                });
            }
            listner.MessageReceived += Listner_MessageReceived;
        }

        private void Listner_MessageReceived(object listener, MessageReceivedEventArgs messageReceivedEventArgs)
        {
            var topic = ((Listener)listener).Subject;

            List<MarketParserModel> parserModels;
            if (!_topicSubscribers.TryGetValue(topic, out parserModels))
                return;

            var message = messageReceivedEventArgs.Message;

            foreach (var parserModel in parserModels)
            {
                var model = parserModel.Parser.GetModelFromMesssage(message);
                parserModel.ReceivedHandler(this, new MarketReceivedEventArgs { Market = model, Topic = topic });
            }
        }

        public void RunListner()
        {
            try
            {
                while (_listenerCancellationToken.IsCancellationRequested == false)
                {
                    Queue.Default.Dispatch();
                }
            }
            catch (Exception)
            {
                // ignored
            }
        }
        public void Dispose()
        {
            _listenerCancellationToken.Cancel();
            _transport.Destroy();
        }
    }
}