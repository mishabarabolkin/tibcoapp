﻿using SignalRApplication.Bll.Models;
using SignalRApplication.Bll.Utils;

namespace SignalRApplication.Bll.TIBcoRv
{
    public class TibcoCommunicator
    {
        private const string Daemon = "172.16.131.37:8700";
        private const string Service = "2002";
        private const string Network = ";239.0.0.2";

        private static TibcoCommunicator _communicator;
        private static TibcoSubscriber _tibco;

        private readonly UserMarketSubscriptions _userMarketSubscriptions;


        public static event Delegates.PriceChangedEventHandler MarketReceived;

        public static TibcoCommunicator Singleton
        {
            get
            {
                if (_communicator == null)
                {
                    lock (Service)
                    {
                        if (_communicator == null)
                        {
                            _communicator = new TibcoCommunicator();
                        }
                    }
                }
                return _communicator;
            }
        }

        private TibcoCommunicator()
        {
            _userMarketSubscriptions = new UserMarketSubscriptions();
        }

        public void StartTibco()
        {
            _tibco = new TibcoSubscriber(Daemon, Service, Network);
            _tibco.StartTibco();
        }

        public void AddTibcoListeners()
        {
            _tibco.AddListners("PKH-DEV-ANCH01.DATA.PRICE.*", PriceReceivedHandler, new MessageParser());
        }

        public void AddMarketSubscriber(string connectionId, int marketId)
        {
            _userMarketSubscriptions.AddMarketSubscriber(connectionId, marketId);
        }

        public void RemoveMarketSubscriber(string connectionId, int? marketId = null)
        {
            _userMarketSubscriptions.RemoveMarketSubscriber(connectionId, marketId);
        }

        public void PriceReceivedHandler(object sender, MarketReceivedEventArgs e)
        {
            var marketSubscription = _userMarketSubscriptions.GetMarketSubscribtions();

            if (!marketSubscription.ContainsKey(e.Market.Id))
                return;

            foreach (var subscriber in marketSubscription[e.Market.Id])
            {
                MarketReceived?.Invoke(this, new PriceChangedEventArgs { Market = e.Market, ConnectionId = subscriber });
            }
        }
    }
}
