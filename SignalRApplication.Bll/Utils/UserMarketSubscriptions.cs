﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace SignalRApplication.Bll.Utils
{
    public class UserMarketSubscriptions
    {
        private readonly ConcurrentDictionary<int, List<string>> _marketSubscribtions = new ConcurrentDictionary<int, List<string>>();

        public ConcurrentDictionary<int, List<string>> GetMarketSubscribtions()
        {
            return _marketSubscribtions;
        }

        public void AddMarketSubscriber(string connectionId, int marketId)
        {
            _marketSubscribtions.AddOrUpdate(marketId,
                                             new List<string> { connectionId },
                                             (key, list) =>
                                             {
                                                 list.Add(connectionId);
                                                 return list;
                                             });
        }

        public void RemoveMarketSubscriber(string connectionId, int? marketId)
        {
            if (marketId.HasValue)
            {
                List<string> userSubscribtions;
                if (_marketSubscribtions.TryGetValue(marketId.Value, out userSubscribtions))
                {
                    _marketSubscribtions[marketId.Value].RemoveAll(s => s == connectionId);
                    if (_marketSubscribtions[marketId.Value].Count == 0)
                    {
                        List<string> users;
                        _marketSubscribtions.TryRemove(marketId.Value, out users);
                    }
                }
            }
            else
            {
                foreach (var record in _marketSubscribtions)
                {
                    record.Value.RemoveAll(v => v == connectionId);

                    if (record.Value.Count == 0)
                    {
                        List<string> users;
                        _marketSubscribtions.TryRemove(record.Key, out users);
                    }
                }
            }
        }

    }
}
