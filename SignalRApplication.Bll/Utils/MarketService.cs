﻿using System.Collections.Generic;
using SignalRApplication.Bll.Models;

namespace SignalRApplication.Bll.Utils
{
    public class MarketService
    {
        public static List<Market> GetAll()
        {
            return new List<Market> {
                new Market { Id = 400494226, MarketName = "EUR/USD"},
                new Market { Id = 154290, MarketName = "EUR/USD (per 0.0001) CFD"},
                new Market { Id = 99500, MarketName = "UK 100 CFD"},
                new Market { Id = 401214568, MarketName = "Wall Street BINARY1 Daily Future Tue Spread"},
                new Market { Id = 99498, MarketName = "Wall Street CFD"},
                new Market { Id = 400615832, MarketName = "Amazon.com DFT"},
                new Market { Id = 400481126, MarketName = "EUR/GBP"}
            };
        }
    }
}
