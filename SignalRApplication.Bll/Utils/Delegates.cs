﻿using SignalRApplication.Bll.Models;

namespace SignalRApplication.Bll.Utils
{
    public class Delegates
    {
        public delegate void MarketReceivedEventHandler(object sender, MarketReceivedEventArgs e);

        public delegate void PriceChangedEventHandler(object sender, PriceChangedEventArgs e);
    }
}
