﻿using System;

namespace SignalRApplication.Bll.Models
{
    public class MarketReceivedEventArgs: EventArgs
    {
        public Market Market { get; set; }

        public string Topic { get; set; }
    }
}
