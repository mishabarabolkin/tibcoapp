﻿
namespace SignalRApplication.Bll.Models
{
    public class Market
    {
        public int Id { get; set; }
        public string MarketName { get; set; }
        public double Bid { get; set; }
        public double Offer { get; set; }
        public bool Direction { get; set; }
    }
}