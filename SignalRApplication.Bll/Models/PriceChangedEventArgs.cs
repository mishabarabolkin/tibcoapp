﻿using System;

namespace SignalRApplication.Bll.Models
{
    public class PriceChangedEventArgs: EventArgs
    {
        public Market Market { get; set; }

        public string ConnectionId{ get; set; }
    }
}
