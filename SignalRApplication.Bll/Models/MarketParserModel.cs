﻿using SignalRApplication.Bll.TIBcoRv;
using SignalRApplication.Bll.Utils;

namespace SignalRApplication.Bll.Models
{
    public class MarketParserModel
    {
        public ITibcoMessageParser Parser { get; set; }

        public Delegates.MarketReceivedEventHandler ReceivedHandler { get; set; }
    }
}
