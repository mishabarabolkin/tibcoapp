﻿using TIBCO.Rendezvous;

namespace SignalRApplication.Bll.Tibco.Parsers
{
    public interface ITibcoMessageParser
    {
        TibcoModel ParseTibcoMessage(Message message);
    }
}