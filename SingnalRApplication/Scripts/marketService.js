﻿(function () {
    angular.module('myApp')
       .service('marketService', ['$http',
            function ($http) {

                var self = this;

                self.getAllMarket = function () {
                    return $http
                        .get('api/Market')
                        .then(function (response) {
                            return response.data;
                        });
                };
            }]);
}());

