﻿(function () {
    angular.module('myApp')
        .controller('myCtrl', ['$scope', 'signalRService', 'marketService',
            function ($scope, signalRService, marketService) {
                $scope.markets = [];
                $scope.isMarketActive = false;

                var clientPushHubProxy = signalRService(signalRService.defaultServer, 'myHub');

                clientPushHubProxy.on('onPriceChanged', function (updatedMarket) {

                    console.log(updatedMarket);

                    var market = $.grep($scope.markets, function (item) {
                        return item.Id === updatedMarket.Id;
                    })[0];

                    var index = $scope.markets.indexOf(market);

                    if (index !== -1) {
                        $scope.markets[index].Bid = updatedMarket.Bid;
                        $scope.markets[index].Offer = updatedMarket.Offer;
                        $scope.markets[index].Direction = updatedMarket.Direction;
                    }
                });

                marketService
                    .getAllMarket()
                    .then(function(data) {
                        $scope.markets = data;
                    });

                $scope.marketChecked = function (market, isMarketActive) {
                    clientPushHubProxy.changeMarketActivity(market.Id, isMarketActive);
                }

            }]);
}());