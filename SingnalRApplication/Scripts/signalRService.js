﻿(function () {
    angular.module('myApp')
       .factory('signalRService', ['$rootScope', 'signalRServer',
    function ($rootScope, signalRServer) {

        function signalRHubProxyFactory(serverUrl, hubName) {
            var connection = $.hubConnection(signalRServer),
                proxy = connection.createHubProxy(hubName),
                topic = 'PKH-DEV-ANCH01.DATA.PRICE.*';

            connection.start().done(function () { });


            return {
                on: function (eventName, callback) {
                    proxy.on(eventName, function (result) {
                        $rootScope.$apply(function () {
                            if (callback) {
                                callback(result);
                            }
                        });
                    });
                },

                invoke: function (methodName) {
                    proxy.invoke(methodName, connection.id);
                },

                changeMarketActivity: function (marketId, isMarketActive) {

                    proxy.invoke('onChangedMarketActivity', marketId, isMarketActive);
                },

                connection: connection
            };
        };
        return signalRHubProxyFactory;
    }]);
}());

