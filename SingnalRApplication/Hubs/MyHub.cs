﻿using Microsoft.AspNet.SignalR;
using SignalRApplication.Bll.TIBcoRv;
using System.Threading.Tasks;
using SignalRApplication.Bll.Models;

namespace SingnalRApplication.Hubs
{
    public class MyHub : Hub
    {
        private readonly IHubContext _hub;

        public MyHub()
        {
            TibcoCommunicator.MarketReceived += OnPriceChanged;
            _hub = GlobalHost.ConnectionManager.GetHubContext<MyHub>();
        }

        protected virtual void OnPriceChanged(object sender, PriceChangedEventArgs e)
        {
            _hub.Clients.Client(e.ConnectionId).onPriceChanged(e.Market);
        }

        public void OnChangedMarketActivity(int marketId, bool isMarketActive)
        {
            var tibcoCommunicator = TibcoCommunicator.Singleton;

            if (isMarketActive)
            {
                tibcoCommunicator.AddMarketSubscriber(Context.ConnectionId, marketId);
            }
            else
            {
                tibcoCommunicator.RemoveMarketSubscriber(Context.ConnectionId, marketId);
            }
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            var tibcoCommunicator = TibcoCommunicator.Singleton;
            tibcoCommunicator.RemoveMarketSubscriber(Context.ConnectionId);

            return base.OnDisconnected(stopCalled);
        }


    }
}