﻿using SignalRApplication.Bll.Utils;
using System.Collections.Generic;
using System.Web.Http;
using SignalRApplication.Bll.Models;

namespace SingnalRApplication.Controllers
{
    public class MarketController : ApiController
    {
        public IEnumerable<Market> Get()
        {
            return MarketService.GetAll();
        }
    }
}
