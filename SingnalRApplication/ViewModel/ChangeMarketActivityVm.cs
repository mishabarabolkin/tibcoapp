﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SingnalRApplication.ViewModel
{
    public class ChangeMarketActivityVm
    {
        public string Topic { get; set; }
        public int MarketId { get; set; }
        public bool IsActive { get; set; }
        public string ConnectionId { get; set; }
    }
}