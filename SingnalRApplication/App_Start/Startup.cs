﻿using Microsoft.Owin;
using Owin;
using SignalRApplication.Bll.TIBcoRv;

[assembly: OwinStartup(typeof(SingnalRApplication.Startup))]

namespace SingnalRApplication
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var communicator = TibcoCommunicator.Singleton;
            communicator.StartTibco();
            communicator.AddTibcoListeners();

            app.MapSignalR();
        }
    }
}
